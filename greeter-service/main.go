package main

import (
	"context"
	"fmt"
	"log"
	"os"

	"github.com/micro/go-micro"

	// enable consul registry
	_ "github.com/micro/go-plugins/registry/consul"

	pb "gitlab.com/phuonghuynh-eng/shippy/greeter-service/proto/greeter"
)

// bump version greeter to 1.0.2

type greeterHandler struct{}

func (g *greeterHandler) Hello(ctx context.Context, req *pb.HelloRequest, res *pb.HelloResponse) error {
	name := req.GetName()
	if name == "" {
		name = "World"
	}
	hostname, err := os.Hostname()
	if err != nil {
		hostname = "#ERR"
	}

	res.Greeting = fmt.Sprintf("Hello %s. The greeting from %s", name, hostname)
	return nil
}

func main() {
	srv := micro.NewService(
		micro.Name("shippy.service.greeter"),
	)
	srv.Init()
	pb.RegisterGreeterHandler(srv.Server(), &greeterHandler{})
	log.Println("starting")
	if err := srv.Run(); err != nil {
		log.Fatal(err)
	}
}
